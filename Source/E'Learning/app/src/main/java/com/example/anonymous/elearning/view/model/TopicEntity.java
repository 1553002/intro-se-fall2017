package com.example.anonymous.elearning.view.model;


import java.io.Serializable;
import java.util.List;

/**
 * Created by CONG_CANH on 12/4/2017.
 */

/*public class TopicEntity {
    String topicName;

    List<WordEntity> level1;
    List<WordEntity> level2;
    List<WordEntity> level3;


    public TopicEntity(){};

    public TopicEntity(String topicName) {
        this.topicName = topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public void setLevel1(List<WordEntity> level1) {
        this.level1 = level1;
    }

    public void setLevel2(List<WordEntity> level2) {
        this.level2 = level2;
    }

    public void setLevel3(List<WordEntity> level3) {
        this.level3 = level3;*/
/**
 * Created by tranquocthaitrieu on 12/5/17.
 */

public class TopicEntity extends BaseEntity implements Serializable{
    String name;
    String key;
    Integer current_level;

    List<WordEntity> level1;

    public TopicEntity() {
    }

    public TopicEntity(String name, String key, Integer current_level) {
        this.name = name;
        this.key = key;
        this.current_level = current_level;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setKey(String key) {
        this.key = key;
    }



    public void setLevel1(List<WordEntity> level1) {
        this.level1 = level1;
    }

    public String getName() {

        return name;
    }

    public String getKey() {
        return key;
    }

    public Integer getCurrent_level() {
        return current_level;
    }

    public void setCurrent_level(Integer current_level) {
        this.current_level = current_level;
    }

    public List<WordEntity> getLevel1() {
        return level1;
    }
}
