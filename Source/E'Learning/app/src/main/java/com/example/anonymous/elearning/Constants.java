package com.example.anonymous.elearning;

/**
 * Created by tranquocthaitrieu on 12/5/17.
 */

public class Constants {
    public static final String EXTRA_TOPIC="topic";
    public static final String EXTRA_QUESTION="question";
    public static final String ACTION_ALARM="com.example.anonymous.elearning.action.alarm";
}
