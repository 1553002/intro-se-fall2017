package com.example.anonymous.elearning.view.activity;

/**
 * Created by Administrator on 12/7/2017.
 */

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.bumptech.glide.Glide;
import com.example.anonymous.elearning.R;
import com.example.anonymous.elearning.service.OnSwipeTouchListener;
import com.example.anonymous.elearning.view.model.TopicEntity;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import android.speech.tts.TextToSpeech;

import java.util.Locale;

public class LearnActivity extends AppCompatActivity implements Toolbar.OnMenuItemClickListener {
    private TextSwitcher spellswitcher;
    private TextSwitcher wordswitcher;
    private ImageView imageswitcher;
    private TextSwitcher defswitcher;
    private ImageButton btnPronounce;
    int cur = 0;
    private Menu menu;
    TopicEntity item;
    String userInfo;
    int level;
    MenuItem mark_item = null;
    ProgressBar progressBar;
    FirebaseAuth mAuth;
    TextToSpeech tts;
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_close);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(LearnActivity.this);
                builder1.setMessage("Do you really want to exit?");
                builder1.setIcon(R.drawable.com_facebook_button_icon_blue);
                builder1.setCancelable(true);
                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        progressBar = (ProgressBar)findViewById(R.id.progress_bar);

        mAuth = FirebaseAuth.getInstance();
        //load du lieu tu file chứa các từ đã đánh dấu vào
        // set các thuocj tính marked của các từ đánh dấu = true;
        Toast.makeText(this, "load du lieu tu file chứa các từ đã đánh dấu vào", Toast.LENGTH_SHORT).show();
        item = (TopicEntity) getIntent().getSerializableExtra("Topic");
        tts=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status!=TextToSpeech.ERROR)
                {
                    tts.setLanguage(Locale.UK);
                }
            }
        });
        userInfo = getIntent().getStringExtra("userInfo");
        level = getIntent().getIntExtra("levelChosen",0);

        btnPronounce=findViewById(R.id.btnpronounce);
        btnPronounce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tts.speak(item.getLevel1().get(cur).getWord(),TextToSpeech.QUEUE_FLUSH,null);
            }
        });
        spellswitcher = findViewById(R.id.spellswitcher);
        spellswitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                TextView textView = new TextView(getApplicationContext());
                textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
                textView.setTextSize(20);
                textView.setTextColor(Color.BLACK);
                return textView;
            }
        });
        wordswitcher = findViewById(R.id.wordswitcher);
        wordswitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                TextView textView = new TextView(getApplicationContext());
                textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
                textView.setTextSize(40);
                textView.setTextColor(Color.BLACK);
                return textView;
            }
        });
        wordswitcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView Tv=(TextView)wordswitcher.getCurrentView();
                String curText=Tv.getText().toString();
                if (item.getLevel1().get(cur).getWord()==curText)
                {
                    wordswitcher.setText(item.getLevel1().get(cur).getMeaning());
                }
                else
                    wordswitcher.setText(item.getLevel1().get(cur).getWord());
            }
        });
        defswitcher = findViewById(R.id.defineswitcher);
        defswitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                TextView textView = new TextView(getApplicationContext());
                textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
                textView.setTextSize(20);
                textView.setTextColor(Color.BLACK);
                return textView;
            }
        });
        imageswitcher = findViewById(R.id.imageSwitcher);
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference gsReference = storage.getReferenceFromUrl(item.getLevel1().get(cur).getRef());
        Glide.with(getApplication())
                .using(new FirebaseImageLoader())
                .load(gsReference)
                .into(imageswitcher);
        //check(item.getLevel1().get(cur).getWord());
        final Animation in2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.in2);
        final Animation out2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.out2);
        View view = getWindow().getDecorView();
        view.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeLeft() {

                wordswitcher.setInAnimation(in2);
                wordswitcher.setOutAnimation(out2);
                defswitcher.setInAnimation(in2);
                defswitcher.setOutAnimation(out2);
                spellswitcher.setInAnimation(in2);
                spellswitcher.setOutAnimation(out2);
                if (cur < item.getLevel1().size()-1) {
                    cur++;
                    progressBar.setProgress(cur+1);
                    wordswitcher.setText(item.getLevel1().get(cur).getWord());
                    defswitcher.setText(item.getLevel1().get(cur).getDefining());
                    spellswitcher.setText(item.getLevel1().get(cur).getSpelling());
                    FirebaseStorage storage = FirebaseStorage.getInstance();
                    StorageReference gsReference = storage.getReferenceFromUrl(item.getLevel1().get(cur).getRef());
                    Glide.with(getApplication())
                            .using(new FirebaseImageLoader())
                            .load(gsReference)
                            .into(imageswitcher);
                    if(cur==item.getLevel1().size()-1)
                    {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(LearnActivity.this);
                        builder1.setMessage("Chúc mừng bạn đã học xong level!");
                        builder1.setIcon(R.drawable.com_facebook_button_icon_blue);
                        builder1.setCancelable(true);
                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        if (item.getCurrent_level() < level) {
                                            Task<Void> databaseReference = FirebaseDatabase.getInstance().getReference("users")
                                                    .child(userInfo).child("process").child(item.getKey())
                                                    .child("current_level").setValue(level);

                                        }

                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }
                    MenuItem count = menu.findItem(R.id.count_word);
                    String tmp=(cur+1)+"/"+item.getLevel1().size();
                    count.setTitle(tmp);
                }

                check(item.getLevel1().get(cur).getWord());

            }
            @Override
            public void onSwipeRight() {
                wordswitcher.setInAnimation(getApplication(), android.R.anim.slide_in_left);
                wordswitcher.setOutAnimation(getApplication(), android.R.anim.slide_out_right);
                defswitcher.setInAnimation(getApplication(), android.R.anim.slide_in_left);
                defswitcher.setOutAnimation(getApplication(), android.R.anim.slide_out_right);
                spellswitcher.setInAnimation(getApplication(), android.R.anim.slide_in_left);
                spellswitcher.setOutAnimation(getApplication(), android.R.anim.slide_out_right);
                if (cur > 0) {
                    cur--;
                    progressBar.setProgress(cur+1);
                    wordswitcher.setText(item.getLevel1().get(cur).getWord());
                    defswitcher.setText(item.getLevel1().get(cur).getDefining());
                    spellswitcher.setText(item.getLevel1().get(cur).getSpelling());
                    FirebaseStorage storage = FirebaseStorage.getInstance();
                    StorageReference gsReference = storage.getReferenceFromUrl(item.getLevel1().get(cur).getRef());
                    Glide.with(getApplication())
                            .using(new FirebaseImageLoader())
                            .load(gsReference)
                            .into(imageswitcher);

                    check(item.getLevel1().get(cur).getWord());
                }
                MenuItem count = menu.findItem(R.id.count_word);
                String tmp=(cur+1)+"/"+item.getLevel1().size();
                count.setTitle(tmp);
            }
        });
        if(cur ==0)
        {
            wordswitcher.setText(item.getLevel1().get(cur).getWord());
            defswitcher.setText(item.getLevel1().get(cur).getDefining());
            spellswitcher.setText(item.getLevel1().get(cur).getSpelling());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.learn_bar,menu);
        this.menu = menu;
        mark_item = menu.getItem(1);

        if(cur ==0)
        {
            if(item.getLevel1().get(cur).getmarked()==true)
                mark_item.setIcon(R.drawable.ic_marked);
            else
            {
                mark_item.setIcon(R.drawable.ic_not_mark);
            };
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem Item) {
        int id=Item.getItemId();

        if(id==R.id.mark_word_icon)
        {
            if(item.getLevel1().get(cur).getmarked() == true)   //unmark word
            {
                deleteSavedWord(item.getLevel1().get(cur).getWord());
            }
            else    //mark word
            {
                saveWord(item.getKey(),level, item.getLevel1().get(cur).getWord());
            }
        }
        return super.onOptionsItemSelected(Item);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId())
        {

        }
        return false;
    }


    //1553002 - 28/12/2017
    public void saveWord(String topic, int level, String word)
    {
        String level_string = null;
        switch (level)
        {
            case 0: case 1:
            level_string = "level1"; break;
            case 2:
                level_string = "level2"; break;
            case 3:
                level_string = "level3"; break;
        }
        String tmp = mAuth.getCurrentUser().getUid();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        ref.child("users").child(mAuth.getCurrentUser().getUid()).child("savedWords").child(word)
                .child("topic").setValue(topic);
        ref.child("users").child(mAuth.getCurrentUser().getUid()).child("savedWords").child(word)
                .child("level").setValue(level_string);
    }

    //1553002 - 28/12/2017
    public void deleteSavedWord(String word)
    {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        ref.child("users").child(mAuth.getUid()).child("savedWords").child(word).removeValue();
    }

    void check(String word) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("users").child(mAuth.getUid())
                .child("savedWords").child(word);
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    mark_item.setIcon(R.drawable.ic_marked);
                    item.getLevel1().get(cur).setmarked(true);
                } else {
                    mark_item.setIcon(R.drawable.ic_not_mark);
                    item.getLevel1().get(cur).setmarked(false);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onBackPressed()
    {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(LearnActivity.this);
        builder1.setMessage("Do you really want to exit?");
        builder1.setIcon(R.drawable.com_facebook_button_icon_blue);
        builder1.setCancelable(true);
        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}