package com.example.anonymous.elearning.view.model;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

/**
 * Created by CONG_CANH on 12/5/2017.
 */

public class UserInfo {
    String email;
    HashMap<String,TopicEntity> process;

    public interface Callback {

        void act(HashMap<String,TopicEntity> models);
    }

    public UserInfo(){};

    public UserInfo(String email) {
        this.email = email;
        process = new HashMap<>();
    }

    public UserInfo(String email, HashMap<String,TopicEntity> process) {
        this.email = email;
        this.process = process;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getEmail() {
        return email;
    }

    public HashMap<String,TopicEntity> getProcess() {
        return process;
    }

    public void setListTopics(final Callback callback)
    {
        final DatabaseReference topicRef = FirebaseDatabase.getInstance().getReference("topics");
        topicRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot topic_entry : dataSnapshot.getChildren())
                {
                    TopicEntity topicEntity = new TopicEntity();
                    topicEntity.setKey(topic_entry.getKey());
                    topicEntity.setCurrent_level(0);
                    topicEntity.setName(topic_entry.child("name").getValue().toString());

                    process.put(topic_entry.getKey(),topicEntity);
                }
                callback.act(process);
                topicRef.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }




}
