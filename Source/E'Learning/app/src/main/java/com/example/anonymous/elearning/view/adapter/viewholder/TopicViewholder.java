package com.example.anonymous.elearning.view.adapter.viewholder;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.anonymous.elearning.R;
import com.example.anonymous.elearning.view.model.TopicEntity;
import com.example.anonymous.elearning.view.model.WordEntity;

/**
 * Created by tranquocthaitrieu on 11/7/17.
 */

public class TopicViewholder extends RecyclerView.ViewHolder {
    private TextView tvTitle;
    private TextView tvDescription;
    private ProgressBar progressBar;
    private AppCompatImageButton btnTest;
    private Context context;
    private int position;
    private TopicViewHolderListener listener;
    private TopicEntity item;

    public interface TopicViewHolderListener{
        void onClickTest(int position);
        void onOpenItemTest(int position);
    }

    public TopicViewholder(View itemView) {
        super(itemView);
    }

    public TopicViewholder(View itemView, TopicViewHolderListener listener) {
        super(itemView);
        this.context = itemView.getContext();
        this.listener = listener;
        this.tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
        this.tvDescription = (TextView) itemView.findViewById(R.id.tv_description);
        this.progressBar = (ProgressBar) itemView.findViewById(R.id.progess_bar);
        this.btnTest = (AppCompatImageButton) itemView.findViewById(R.id.btn_test);
        Typeface fontWord = Typeface.createFromAsset(context.getAssets(),context.getString(R.string.font1));
        tvTitle.setTypeface(fontWord);
        Typeface fontDefining = Typeface.createFromAsset(context.getAssets(),context.getString(R.string.font1));
        tvDescription.setTypeface(fontDefining);
        itemView.setOnClickListener(onClickListener);
        btnTest.setOnClickListener(onClickTestListener);
    }

    public void renderUi(int position, TopicEntity item) {
        this.position = position;
        this.item = item;
        tvTitle.setText(item.getName());
        progressBar.setMax(context.getResources().getInteger(R.integer.MAX_LEVEL));
        progressBar.setProgress(item.getCurrent_level());
        tvDescription.setText(String.format(context.getString(R.string.topic_progress_pattern),item.getCurrent_level(), context.getResources().getInteger(R.integer.MAX_LEVEL)));
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            listener.onOpenItemTest(position);
        }
    };

    View.OnClickListener onClickTestListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            listener.onClickTest(position);
        }
    };

}