package com.example.anonymous.elearning.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.anonymous.elearning.R;
import com.example.anonymous.elearning.helper.AlarmDBHelper;
import com.example.anonymous.elearning.helper.AlarmManagerHelper;
import com.example.anonymous.elearning.view.activity.AlarmDetailsActivity;
import com.example.anonymous.elearning.view.adapter.AlarmListAdapter;
import com.example.anonymous.elearning.view.model.AlarmModel;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Cong Canh on 12/13/2017.
 */

public class SetTimeFragment extends Fragment {

    private AlarmListAdapter mAdapter;
    private AlarmDBHelper dbHelper ;
    private Context mContext;
    ListView list;
    FloatingActionButton btn_add;
    Fragment mFragment;
    View mContentView=null;

    public static SetTimeFragment newInstance() {

        Bundle args = new Bundle();

        SetTimeFragment fragment = new SetTimeFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        dbHelper=new AlarmDBHelper(activity);
        mContext=activity;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_alarm, container, false);
        btn_add=(FloatingActionButton) view.findViewById(R.id.add_alarm_btn);
        mAdapter = new AlarmListAdapter(getActivity(), dbHelper.getAlarms(),this);
        list=(ListView) view.findViewById(R.id.list_alarm);
        list.setAdapter(mAdapter);

        //Không hiện tiêu đề
        // getSupportActionBar().setDisplayShowTitleEnabled(false);
        //Hiện nút back
        // getSupportActionBar().setTitle("Set Time");
        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startAlarmDetailsActivity(-1);
            }
        });

        ActionBar actionbar=((AppCompatActivity)getActivity()).getSupportActionBar();
        actionbar.setTitle("Set Time");

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            mAdapter.setAlarms(dbHelper.getAlarms());
            mAdapter.notifyDataSetChanged();
        }
    }

    public void setAlarmEnabled(long id, boolean isEnabled) {
        AlarmManagerHelper.cancelAlarms(getActivity());

        AlarmModel model = dbHelper.getAlarm(id);
        model.setEnabled(isEnabled);
        dbHelper.updateAlarm(model);

        mAdapter.setAlarms(dbHelper.getAlarms());
        mAdapter.notifyDataSetChanged();

        AlarmManagerHelper.setAlarms(getActivity());
    }

    public void startAlarmDetailsActivity(long id) {
        Intent intent = new Intent(getActivity(), AlarmDetailsActivity.class);
        intent.putExtra("id", id);
        startActivityForResult(intent, 0);
    }

    public void deleteAlarm(long id) {
        final long alarmId = id;
        Log.i(null, "1234567");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Do you want delete?").setTitle("Truely set?").setCancelable(true)
                .setNegativeButton("Cancel", null)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        dbHelper.deleteAlarm(alarmId);
                        mAdapter.setAlarms(dbHelper.getAlarms());
                        mAdapter.notifyDataSetChanged();
                    }
                }).show();
    }
}
