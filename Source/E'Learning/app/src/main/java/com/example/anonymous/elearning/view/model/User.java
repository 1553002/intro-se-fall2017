package com.example.anonymous.elearning.view.model;

import java.util.HashMap;
import java.util.List;

/**
 * Created by CONG_CANH on 12/6/2017.
 */

public class User {
    String email;
    String name;
    List<HashMap<String,String>> history;

    public User(){};

    public User(String email, String name) {
        this.email = email;
        this.name = name;
    }


    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public List<HashMap<String, String>> getHistory() {
        return history;
    }
}
