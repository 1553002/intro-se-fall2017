package com.example.anonymous.elearning.helper;

import android.app.Fragment;
import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;

import com.example.anonymous.elearning.R;
import com.example.anonymous.elearning.view.adapter.viewholder.MarkWordListAdapter;
import com.example.anonymous.elearning.view.model.SavedWord;
import com.example.anonymous.elearning.view.model.WordEntity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CONG_CANH on 12/4/2017.
 */

public class GetDataFromFirebase implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {
    String TAG = "GetDataFromFirebase";
    String userID;
    Context mContext;
    Fragment mFragment;
    View view;
    public  List<WordEntity> listWords;
    ArrayList<WordEntity> wordEntityArrayList;
    private MarkWordListAdapter mMarkWordListAdapter;
    public GetDataFromFirebase(View v){view = v;};
    public GetDataFromFirebase(Fragment mFragment) {
        this.mFragment = mFragment;
    }

    public interface Callback {

        void act(List<WordEntity> models);
    }

    /*1553002 - Lay toan bo tu cua 1 topic*/
    public void getDataLevel(final String topic, final String level, final Callback callback) {
        listWords = new ArrayList<>();
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference("topics").child(topic)
                .child(level);
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot wordIndex : dataSnapshot.getChildren()) {
                        final WordEntity curWord = wordIndex.getValue(WordEntity.class);
                        listWords.add(curWord);
                    }

                    callback.act(listWords);
                    ref.removeEventListener(this);
                } else {
                    Log.v(TAG, "Get data finished");
                    ref.removeEventListener(this);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        ref.addListenerForSingleValueEvent(valueEventListener);

        //ref.removeEventListener(valueEventListener);
        Log.v(TAG, "Here");
    }

    //1553002 - Lay toan bo nhung tu da saved
    public void getSavedWord(final Context context, String userID)
    {
        this.userID = userID;
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users")
                .child(userID).child("savedWords");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                wordEntityArrayList = new ArrayList<>();
                setupRecycleview(new MarkWordListAdapter(context, wordEntityArrayList), context);
                if (dataSnapshot.exists()) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        final SavedWord savedWord = child.getValue(SavedWord.class);
                        savedWord.setWord(child.getKey());

                        DatabaseReference word_ref = FirebaseDatabase.getInstance()
                                .getReference("topics")
                                .child(savedWord.getTopic())
                                .child(savedWord.getLevel()).child(savedWord.getWord());

                        word_ref.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                WordEntity temp = dataSnapshot.getValue(WordEntity.class);
                                temp.setmarked(true);
                                wordEntityArrayList.add(temp);
                                mMarkWordListAdapter = new MarkWordListAdapter(context, wordEntityArrayList);
                                setupRecycleview(mMarkWordListAdapter, context);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public int getSize() {
        return listWords.size();
    }

    public void getAttendants(final String topic, final String level) {

        getDataLevel(topic, level, new Callback() {
            @Override
            public void act(List<WordEntity> models) {
                Log.d("attendees", String.valueOf(models.size()));
            }
        });
    }


    private void setupRecycleview(MarkWordListAdapter markWordListAdapter, Context context)
    {
        markWordListAdapter.notifyDataSetChanged();
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.marked_recycle_view);

        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(markWordListAdapter);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);

        //prepareCart();
    }



    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof MarkWordListAdapter.RecyclerViewHolder) {
            // get the removed item name to display it in snack bar
            String name = wordEntityArrayList.get(viewHolder.getAdapterPosition()).getWord();

            // backup of removed item for undo purpose
            final WordEntity deletedItem = wordEntityArrayList.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view
            mMarkWordListAdapter.removeItem(viewHolder.getAdapterPosition());

            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users");
            ref.child(userID).child("savedWords").child(deletedItem.getWord()).removeValue();
            // showing snack bar with Undo option
/*            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, name + " removed from cart!", Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // undo is selected, restore the deleted item
                    mAdapter.restoreItem(deletedItem, deletedIndex);
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();*/
        }
    }
}
