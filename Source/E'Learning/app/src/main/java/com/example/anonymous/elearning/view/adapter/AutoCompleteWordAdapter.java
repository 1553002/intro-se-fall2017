package com.example.anonymous.elearning.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.example.anonymous.elearning.R;
import com.example.anonymous.elearning.view.model.WordEntity;

import java.util.ArrayList;

/**
 * Created by nhan on 12/29/2017.
 */

public class AutoCompleteWordAdapter extends ArrayAdapter<WordEntity> {
    ArrayList<WordEntity> customers, tempWords, suggestions;

    public AutoCompleteWordAdapter(Context context, ArrayList<WordEntity> objects) {
        super(context, android.R.layout.simple_list_item_1, objects);
        this.customers = objects;
        this.tempWords = new ArrayList<WordEntity>(objects);
        this.suggestions = new ArrayList<WordEntity>(objects);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        WordEntity customer = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.base_row, parent, false);
        }
        TextView tvWord = (TextView) convertView.findViewById(R.id.tv_word);
        TextView tvAnoun =(TextView) convertView.findViewById(R.id.tv_anoun);
        TextView tvDefine=(TextView) convertView.findViewById(R.id.tv_defining);

        //ImageView ivCustomerImage = (ImageView) convertView.findViewById(R.id.ivCustomerImage);

        if (tvWord != null)
            tvWord.setText(customer.getWord());
        if(tvAnoun!=null)
            tvAnoun.setText(customer.getSpelling());
        if(tvDefine!=null)
            tvDefine.setText(customer.getDefining());

        //set image vao nha ong
      //  if (ivCustomerImage != null && customer.getProfilePic() != -1)
          //  ivCustomerImage.setImageResource(customer.getProfilePic());


        return convertView;
    }


    @Override
    public Filter getFilter() {
        return myFilter;
    }

    Filter myFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            WordEntity myword = (WordEntity) resultValue;
            return myword.getWord();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (WordEntity words : tempWords) {
                    if (words.getWord().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(words);
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<WordEntity> c = (ArrayList<WordEntity>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (WordEntity cust : c) {
                    add(cust);
                    notifyDataSetChanged();
                }
            }
        }
    };
}