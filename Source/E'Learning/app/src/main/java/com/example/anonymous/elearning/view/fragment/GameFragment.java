package com.example.anonymous.elearning.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.anonymous.elearning.Constants;
import com.example.anonymous.elearning.R;
import com.example.anonymous.elearning.view.model.QuestionEntity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Trieu on 09/11/2017.
 */

public class GameFragment extends BaseFragmentv4 {

    @BindView(R.id.tv_request)
    TextView tvRequest;
    @BindView(R.id.tv_quesiton)
    TextView tvQuesiton;
    @BindView(R.id.edt_answer)
    EditText edtAnswer;
    Unbinder unbinder;
    private Context context;

    private QuestionEntity mQuestion;

    public GameFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TrangChuFragment.
     */
    public static GameFragment newInstance(QuestionEntity item) {
        GameFragment fragment = new GameFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.EXTRA_QUESTION, item);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mQuestion = (QuestionEntity) getArguments().get(Constants.EXTRA_QUESTION);
    }

    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_game;
    }

    @Override
    protected void onMyCreateView() {
        tvRequest.setText(mQuestion.getRequest());
        tvQuesiton.setText(mQuestion.getQuestion());
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
