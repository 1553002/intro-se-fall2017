package com.example.anonymous.elearning.view.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.anonymous.elearning.R;
import com.example.anonymous.elearning.helper.AlarmDBHelper;
import com.example.anonymous.elearning.helper.AlarmManagerHelper;
import com.example.anonymous.elearning.view.model.AlarmModel;


/**
 * Created by nhan on 12/1/2017.
 */

public class AlarmDetailsActivity extends AppCompatActivity {
    private AlarmModel alarmDetails;
    AlarmDBHelper dbHelper = new AlarmDBHelper(this);


    private EditText editName;
    private CheckBox chkSunday;
    private CheckBox chkMonday;
    private CheckBox chkTuesday;
    private CheckBox chkWednesday;
    private CheckBox chkThursday;
    private CheckBox chkFriday;
    private CheckBox chkSaturday;

    int curhour=0;
    int currminute=0;
    boolean isAM=true;
    AppCompatButton save_btn;
    Spinner hour;
    String[] hours={"hour","1","2","3","4","5","6","7","8","9","10","11","12"};

    Spinner minute;
    String[] minutes={"minute","00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16"
            ,"17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"
            ,"32","33","34","35","36","37","38","39","40","41","42","43","44","45","46"
            ,"47","48","49","50","51","52","53","54","55","56","57","58","59"};

    Spinner AM;
    String[] AMs={"AM","PM"};
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_ACTION_BAR);

        setContentView(R.layout.activity_alarm_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        hour=(Spinner) findViewById(R.id.hour);
        minute=(Spinner) findViewById(R.id.minute);
        AM=(Spinner) findViewById(R.id.AM);
        ArrayAdapter<String> adapterhour=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,hours);
        ArrayAdapter<String> adapterminute=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,minutes);
        ArrayAdapter<String> adapterAm=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,AMs);
        hour.setAdapter(adapterhour);
        minute.setAdapter(adapterminute);
        AM.setAdapter(adapterAm);
        hour.setSelection(0);
        minute.setSelection(0);
        AM.setSelection(0);

        hour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(!hours[position].equals("hour"))
                {
                    curhour=Integer.parseInt(hours[position]);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        minute.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(!minutes[position].equals("minute"))
                {
                    currminute=Integer.parseInt(minutes[position]);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        AM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(AMs[position].equals("AM"))
                {
                    isAM=true;
                }
                else
                {
                    isAM=false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        chkSunday = (CheckBox) findViewById(R.id.alarm_details_repeat_sunday);
        chkMonday = (CheckBox) findViewById(R.id.alarm_details_repeat_monday);
        chkTuesday = (CheckBox) findViewById(R.id.alarm_details_repeat_tuesday);
        chkWednesday = (CheckBox) findViewById(R.id.alarm_details_repeat_wednesday);
        chkThursday = (CheckBox) findViewById(R.id.alarm_details_repeat_thursday);
        chkFriday = (CheckBox) findViewById(R.id.alarm_details_repeat_friday);
        chkSaturday = (CheckBox) findViewById(R.id.alarm_details_repeat_saturday);


        long id = getIntent().getExtras().getLong("id");

        if (id == -1) {
            alarmDetails = new AlarmModel();
        } else {
            alarmDetails = dbHelper.getAlarm(id);


            loadtime(alarmDetails.getTimeHour(),alarmDetails.getTimeMinute()); // load created alarm
            hour.setSelection(curhour);
            minute.setSelection(currminute+1);

            if(isAM) AM.setSelection(0);
            else AM.setSelection(1);

            chkSunday.setChecked(alarmDetails.getRepeatingDay(AlarmModel.SUNDAY));
            chkMonday.setChecked(alarmDetails.getRepeatingDay(AlarmModel.MONDAY));
            chkTuesday.setChecked(alarmDetails.getRepeatingDay(AlarmModel.TUESDAY));
            chkWednesday.setChecked(alarmDetails.getRepeatingDay(AlarmModel.WEDNESDAY));
            chkThursday.setChecked(alarmDetails.getRepeatingDay(AlarmModel.THURSDAY));
            chkFriday.setChecked(alarmDetails.getRepeatingDay(AlarmModel.FRDIAY));

        }
        save_btn=(AppCompatButton) findViewById(R.id.save_alarm_btn);
        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateModelFromLayout();
                Log.i("abc123", "save alarmed");

                AlarmManagerHelper.cancelAlarms(AlarmDetailsActivity.this);

                if (alarmDetails.getId() < 0) {
                    dbHelper.createAlarm(alarmDetails);
                } else {
                    dbHelper.updateAlarm(alarmDetails);
                }

                AlarmManagerHelper.setAlarms(AlarmDetailsActivity.this);

                setResult(RESULT_OK);
                finish();
            }
        });


    }





    private void updateModelFromLayout() {
        alarmDetails.setTimeMinute(currminute);
        tranferAMorPM(isAM);
        alarmDetails.setTimeHour(curhour);



        alarmDetails.setRepeatingDay(AlarmModel.SUNDAY, chkSunday.isChecked());
        alarmDetails.setRepeatingDay(AlarmModel.MONDAY, chkMonday.isChecked());
        alarmDetails.setRepeatingDay(AlarmModel.TUESDAY, chkTuesday.isChecked());
        alarmDetails.setRepeatingDay(AlarmModel.WEDNESDAY, chkWednesday.isChecked());
        alarmDetails.setRepeatingDay(AlarmModel.THURSDAY, chkThursday.isChecked());
        alarmDetails.setRepeatingDay(AlarmModel.FRDIAY, chkFriday.isChecked());
        alarmDetails.setRepeatingDay(AlarmModel.SATURDAY, chkSaturday.isChecked());

        alarmDetails.setEnabled(true);
    }

    void tranferAMorPM(boolean isAm) // tranfer to true time from AM or PM
    {
        if(isAm)
        {
            if(curhour>=12 && curhour<=23)
            {
                curhour=curhour-12;

            }
        }
        else
        {
            if(curhour>=1 && curhour<12)
            {
                curhour=curhour+12;
            }
        }
    }
    void loadtime(int hour, int minute)
    {
        currminute=minute;
        if(hour==0)
        {
            curhour = 12;
            isAM=true;
        }
        else
        {
            if(hour>=1 && hour<=11)
            {
                curhour=hour;
                isAM=true;
            }
            else
            {
                if(hour==12)
                {
                    curhour=hour;
                    isAM=false;
                }
                else
                {
                    if(hour<=23)
                    {
                        curhour=hour-12;
                        isAM=false;
                    }
                }
            }
        }



    }

}
