package com.example.anonymous.elearning.view.adapter.viewholder;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.anonymous.elearning.R;
import com.example.anonymous.elearning.view.model.WordEntity;

/**
 * Created by tranquocthaitrieu on 11/7/17.
 */

public class FlashCardViewholder extends RecyclerView.ViewHolder {
    private TextView tvWord;
    private TextView tvDefining;
    private Context context;
    private int position;
    private ItemClickListener listener;
    private WordEntity item;

    public FlashCardViewholder(View itemView) {
        super(itemView);
    }

    public FlashCardViewholder(View itemView, ItemClickListener listener) {
        super(itemView);
        this.context = itemView.getContext();
        this.listener = listener;
        this.tvWord = (TextView) itemView.findViewById(R.id.tv_word);
        this.tvDefining = (TextView) itemView.findViewById(R.id.tv_defining);
        Typeface fontWord = Typeface.createFromAsset(context.getAssets(),"fonts/helveticaneuecondensedbold.ttf");
        tvWord.setTypeface(fontWord);
        Typeface fontDefining = Typeface.createFromAsset(context.getAssets(),"fonts/helveticaneuecondensedbold.ttf");
        tvDefining.setTypeface(fontDefining);
        itemView.setOnClickListener(onClickListener);
    }

    public void renderUi(int position, WordEntity item) {
        this.position = position;
        this.item = item;
        tvWord.setText(item.getWord());
        tvDefining.setText(item.getDefining());
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            listener.OnClickItem(position);
        }
    };

}
