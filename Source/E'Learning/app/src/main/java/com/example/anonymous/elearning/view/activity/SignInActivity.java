package com.example.anonymous.elearning.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.anonymous.elearning.R;
import com.example.anonymous.elearning.helper.GetDataFromFirebase;
import com.example.anonymous.elearning.view.model.TopicEntity;
import com.example.anonymous.elearning.view.model.UserInfo;
import com.example.anonymous.elearning.view.model.WordEntity;
import com.facebook.login.widget.LoginButton;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.firebase.ui.auth.ResultCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;

/**
 * Created by tranquocthaitrieu on 12/1/17.
 */
public class SignInActivity extends AppCompatActivity{

    //private CallbackManager callbackManager;
    public static final String TAG = "SignInActivity";
    private FirebaseAuth mAuth;
    FirebaseUser currentUser;
    // Choose an arbitrary request code value
    private static final int RC_SIGN_IN = 123;



    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Realm.getInstance(this); //initialize other plugins
        setContentView(R.layout.activity_sign_in);

        mAuth = FirebaseAuth.getInstance();

        Button loginButton = findViewById(R.id.btn_loginFacebook);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<AuthUI.IdpConfig> providers = Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.FACEBOOK_PROVIDER).build());
                startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder().
                        setAvailableProviders(providers).
                        setIsSmartLockEnabled(false)
                        .build(), RC_SIGN_IN);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // RC_SIGN_IN is the request code you passed into startActivityForResult(...) when starting the sign in flow.
        Log.d("Debug", "SIGN_IN");
        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            // Successfully signed in
            if (resultCode == RESULT_OK) {
                Log.d("Debug", "OK");
                findViewById(R.id.btn_loginFacebook).setVisibility(View.GONE);


                setupFirstTimeUse();


                startActivity(new Intent(this, WelcomeActivity.class));
                finish();
                return;
            } else {
                // Sign in failed
                if (response == null) {
                    // User pressed back button
                    showToast(getString(R.string.sign_in_cancelled));
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.NO_NETWORK) {
                    showToast(getString(R.string.no_internet_connection));
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                    //showSnackbar(R.string.unknown_error);
                    return;
                }
            }

            //showSnackbar(R.string.unknown_sign_in_response);
        }
    }



    private void showToast(String message) {
        Snackbar.make(findViewById(R.id.myLinearLayout), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        currentUser = mAuth.getCurrentUser();
        if (currentUser != null)
        {
            startActivity(new Intent(this, WelcomeActivity.class));
            finish();
            return;
        }
    }

    /*1553002 - 10/Dec/2017
    * Thiet lap cho nguoi dung lan dau tien dang ky tai khoan su dung app*/
    private void setupFirstTimeUse()
    {
        final DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("users")
                .child(mAuth.getCurrentUser().getUid());
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists())
                {
                    Log.v(TAG, "Da ton tai tai khoan. Khong khoi tao gia tri ban dau");
                }
                else
                {
                    final UserInfo new_user = new UserInfo(mAuth.getCurrentUser().getEmail());
                    new_user.setListTopics(new UserInfo.Callback() {
                        @Override
                        public void act(HashMap<String, TopicEntity> models) {
                            FirebaseDatabase.getInstance().getReference("users").child(mAuth.getCurrentUser().getUid())
                                    .setValue(new_user);
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
