package com.example.anonymous.elearning.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.example.anonymous.elearning.Constants;
import com.example.anonymous.elearning.helper.AlarmManagerHelper;
import com.example.anonymous.elearning.view.activity.AlarmScreen;

/**
 * Created by nhan on 12/1/2017.
 */

public class AlarmService extends Service {
    public static String TAG = AlarmService.class.getSimpleName();

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        if (intent.getAction().equals(Constants.ACTION_ALARM)) {
            Intent alarmIntent = new Intent(getBaseContext(), AlarmScreen.class);
            alarmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            alarmIntent.putExtras(intent);
            getApplication().startActivity(alarmIntent);
            AlarmManagerHelper.setAlarms(this);
        }
        return super.onStartCommand(intent, flags, startId);
    }
}
