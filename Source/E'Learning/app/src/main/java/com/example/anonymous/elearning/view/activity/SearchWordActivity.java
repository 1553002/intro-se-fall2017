package com.example.anonymous.elearning.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.anonymous.elearning.R;
import com.example.anonymous.elearning.view.adapter.AutoCompleteWordAdapter;
import com.example.anonymous.elearning.view.model.WordEntity;

import java.util.ArrayList;

/**
 * Created by nhan on 12/29/2017.
 */

public class SearchWordActivity extends AppCompatActivity implements  Toolbar.OnMenuItemClickListener{

    TextView Word,Define,Anoun;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.search_word_activity);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_close);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("Search Marked Words");

        Bundle bundle=getIntent().getBundleExtra("BUNDLE");
        String word_search=bundle.get("Word").toString();


       Word=(TextView) findViewById(R.id.tv_word);
       Define=(TextView) findViewById(R.id.tv_defining);
       Anoun=(TextView) findViewById(R.id.tv_anoun);
       imageView=(ImageView) findViewById(R.id.iv);

       //tao mang danh sach tu WordEntuty
        WordEntity word=new WordEntity();
        word.setBaseWord("Red","do","/aa/");
        WordEntity word1=new WordEntity();
        word1.setBaseWord("Green","xanh","/bb/");
        WordEntity word2=new WordEntity();
        word2.setBaseWord("Blue","xanh troi","/ggg/");

        final ArrayList<WordEntity> dogList= new ArrayList<>();
        dogList.add(word);
        dogList.add(word1);
        dogList.add(word2);

        // tao adapter
        AutoCompleteWordAdapter adapter = new AutoCompleteWordAdapter(this, dogList);

        final AutoCompleteTextView autocompleteView = (AutoCompleteTextView) findViewById(R.id.autocompleteView);
        //set adapter
        autocompleteView.setAdapter(adapter);
        autocompleteView.setText(word_search);
        autocompleteView.setSelection(autocompleteView.getText().length());

        autocompleteView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                WordEntity word=dogList.get(position);
                //autocompleteView.setText(word.getWord());

                //load word len
                Word.setText(word.getWord());
                Define.setText(word.getDefining());
                Anoun.setText(word.getSpelling());
                //set them  imageView vao nhe layout la base row ak


            }
        });



    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }
}
