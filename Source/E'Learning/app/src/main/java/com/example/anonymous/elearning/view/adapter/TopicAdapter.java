package com.example.anonymous.elearning.view.adapter;

import android.app.Fragment;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.anonymous.elearning.R;
import com.example.anonymous.elearning.helper.GetDataFromFirebase;
import com.example.anonymous.elearning.view.adapter.viewholder.TopicViewholder;
import com.example.anonymous.elearning.view.fragment.HomeFragment;
import com.example.anonymous.elearning.view.model.TopicEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tranquocthaitrieu on 11/7/17.
 */

public class TopicAdapter extends RecyclerView.Adapter<TopicViewholder> implements TopicViewholder.TopicViewHolderListener{
    private Fragment fragment;
    private List<TopicEntity> dataSet;
    private LayoutInflater mLayoutInflater;

    AlertDialog alertDialog1;
    public TopicAdapter(Fragment fragment) {
        this.fragment = fragment;
        this.mLayoutInflater = LayoutInflater.from(fragment.getActivity());
        this.dataSet=new ArrayList<>();
    }

    public void setData(List<TopicEntity> dataSet){
        if (this.dataSet==null)
            this.dataSet=new ArrayList<>();
        this.dataSet.clear();
        this.dataSet.addAll(dataSet);
        notifyDataSetChanged();
    }
    public List<TopicEntity> getData()
    {
        return dataSet;
    }


    @Override
    public TopicViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.cardview_topic, parent, false);
        TopicViewholder holder = new TopicViewholder(view,this);
        return holder;
    }

    @Override
    public void onBindViewHolder(TopicViewholder holder, int position) {
        holder.renderUi(position,dataSet.get(position));

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public void onClickTest(int position) {
        if (fragment instanceof HomeFragment)
            ((HomeFragment)fragment).openTest(dataSet.get(position));
    }

    @Override
    public void onOpenItemTest(int position) {
        final int tmp=position;
        AlertDialog.Builder builder = new AlertDialog.Builder(fragment.getActivity());
        CharSequence[] values = new CharSequence[0];
        switch (dataSet.get(position).getCurrent_level())
        {
            case 0:
                values = new String[1];
                values[0] = "Lesson 1";
                break;
            case 1:
                values = new String[2];
                values[0] = "Lesson 1";
                values[1] = "Lesson 2";
                break;
            case 2: case 3:
            values = new String[3];
            values[0] = "Lesson 1";
            values[1] = "Lesson 2";
            values[2] = "Lesson 3";
            break;
        }

        builder.setTitle("Select Your Choice");

        builder.setSingleChoiceItems(values, -1, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {

                switch(item)
                {
                    case 0:
                        if (fragment instanceof HomeFragment)
                            ((HomeFragment)fragment).openTopic(dataSet.get(tmp),1);
                        break;
                    case 1:
                        if (fragment instanceof HomeFragment)
                            ((HomeFragment)fragment).openTopic(dataSet.get(tmp),2);
                        break;
                    case 2:
                        if (fragment instanceof HomeFragment)
                            ((HomeFragment)fragment).openTopic(dataSet.get(tmp),3);
                        break;
                }
                alertDialog1.dismiss();
            }
        });
        alertDialog1 = builder.create();
        alertDialog1.show();
    }
}