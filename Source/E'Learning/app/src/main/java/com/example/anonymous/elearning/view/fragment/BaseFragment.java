package com.example.anonymous.elearning.view.fragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.anonymous.elearning.view.activity.BaseActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by trieu on 8/11/2017.
 */
public abstract class BaseFragment extends Fragment {
    public static final String TAG = BaseFragment.class.getSimpleName();
    public static final int DELAY = 250; //ms
    private Unbinder mUnbinder;
    View myView;

    @TargetApi(23)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onAttachToContext(context);
    }

    /**
     * deprecated on API 23
     * use onAttachToContext instead
     * @param activity
     */
    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < 23) {
            onAttachToContext(activity);
        }
    }

    protected abstract void onAttachToContext(Context context);

    protected abstract int getLayoutId();

    protected abstract void onMyCreateView();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        mUnbinder = ButterKnife.bind(this, view);
        myView=view;
        onMyCreateView();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    public void showBWarningMessage(String message) {
        Activity act = getActivity();
        if (act instanceof BaseActivity) {
            ((BaseActivity) act).showToastMessage(message);
        }
    }


    public void showBLoadingView(String msg) {
        Activity act = getActivity();
        if (act instanceof BaseActivity) {
            ((BaseActivity) act).showLoadingDialog(msg);
        }
    }

    public void hideBLoadingView() {
        Activity act = getActivity();
        if (act instanceof BaseActivity) {
            ((BaseActivity) act).hideLoadingDialog();
        }
    }

    public void showBWarningDialog(String msg) {
        Activity act = getActivity();
        if (act instanceof BaseActivity) {
            ((BaseActivity) act).showAlertDialog(msg);
        }
    }
}
