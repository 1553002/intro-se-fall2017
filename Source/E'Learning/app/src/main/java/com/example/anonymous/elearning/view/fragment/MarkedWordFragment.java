package com.example.anonymous.elearning.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.anonymous.elearning.R;
import com.example.anonymous.elearning.helper.GetDataFromFirebase;
import com.example.anonymous.elearning.view.activity.SearchWordActivity;
import com.google.firebase.auth.FirebaseAuth;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by nhan on 12/19/2017.
 */

public class MarkedWordFragment extends android.app.Fragment{
    public static final String TAG = MarkedWordFragment.class
            .getSimpleName();
    private Context mContext;
    private RequestQueue mRequestQueue;
    EditText edit;
    boolean isChange=true;

    public static MarkedWordFragment newInstance() {

        Bundle args = new Bundle();

        MarkedWordFragment fragment = new MarkedWordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext=activity;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_marked_words, container, false);
        GetDataFromFirebase getDataFromFirebase = new GetDataFromFirebase(view);
        getDataFromFirebase.getSavedWord(getApplicationContext(), FirebaseAuth.getInstance().getUid());

        setHasOptionsMenu(true);

        edit=(EditText) view.findViewById(R.id.edit_search_word);
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(isChange)
                {   isChange=false;
                    Intent intent = new Intent(getActivity(), SearchWordActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("Word",edit.getText().toString());
                    edit.setText("");
                    intent.putExtra("BUNDLE",bundle);
                    startActivity(intent);
                    isChange=true;
                }
            }
        });
        return view;
    }


    @Override
    public void onStop() {
        super.onStop();
        //cap nhat lai danh sach xuong file
    }


}