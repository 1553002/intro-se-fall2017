package com.example.anonymous.elearning.view.activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.anonymous.elearning.Constants;
import com.example.anonymous.elearning.R;
import com.example.anonymous.elearning.view.model.QuestionEntity;
import com.example.anonymous.elearning.view.model.TopicEntity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class GameActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.heart_bar)
    RatingBar heartBar;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.btn_check)
    Button btnCheck;
    @BindView(R.id.tv_request)
    TextView tvRequest;
    @BindView(R.id.tv_quesiton)
    TextView tvQuesiton;
    @BindView(R.id.edt_answer)
    EditText edtAnswer;
    @BindView(R.id.btn_continue)
    Button btnContinue;
    @BindView(R.id.layout_loss)
    RelativeLayout layoutLoss;
    @BindView(R.id.layout_game)
    LinearLayout layoutGame;
    @BindView(R.id.btn_finish)
    Button btnFinish;
    @BindView(R.id.layout_win)
    RelativeLayout layoutWin;

    private int questionIndex = 0;
    private int HEARTS = 3;
    private FirebaseAuth mAuth;
    private TopicEntity item;
    private List<QuestionEntity> mQuestions;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_game;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prepareData();
        prepareToolbar();
        prepareUI();
    }

    private void prepareUI() {
        layoutGame.setVisibility(View.VISIBLE);
        layoutLoss.setVisibility(View.GONE);
        layoutWin.setVisibility(View.GONE);
        progressBar.getProgressDrawable().setColorFilter(getResources().getColor(R.color.amber_500), PorterDuff.Mode.SRC_IN);
    }

    private void prepareToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(item.getName());
    }

    private void prepareData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
            item = (TopicEntity) bundle.get(Constants.EXTRA_TOPIC);
        mAuth = FirebaseAuth.getInstance();
        showLoadingDialog("Loading...");
        DatabaseReference authRef = FirebaseDatabase.getInstance()
                .getReference("topics").child(item.getKey()).child("games");
        authRef.addValueEventListener(new ValueEventListener() {
            @SuppressLint("ResourceAsColor")
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideLoadingDialog();
                mQuestions = new ArrayList<>();
                for (DataSnapshot topic_entry : dataSnapshot.getChildren()) {
                    mQuestions.add(new QuestionEntity(topic_entry.child("question").getValue().toString(), topic_entry.child("request").getValue().toString()
                            , topic_entry.child("answer").getValue().toString()));
                }
                Collections.shuffle(mQuestions);
                progressBar.setMax(mQuestions.size());
                if (!mQuestions.isEmpty())
                    loadQuestion();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideLoadingDialog();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                showAlertDialog();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        showAlertDialog();
    }

    //Le Cong Canh - 25/12/2017
    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.exit);
        builder.setMessage(R.string.exit_confirm);
        builder.setCancelable(false);
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void loadQuestion() {
        if (questionIndex==mQuestions.size()){
            hideKeyboard();
            layoutGame.setVisibility(View.GONE);
            layoutWin.setVisibility(View.VISIBLE);
            layoutLoss.setVisibility(View.GONE);
            return;
        }
        progressBar.setProgress(questionIndex);
        QuestionEntity mQuestion = mQuestions.get(questionIndex);
        tvRequest.setText(mQuestion.getRequest());
        tvQuesiton.setText(mQuestion.getQuestion());
        questionIndex++;
    }

    @OnClick(R.id.btn_check)
    public void onViewClicked() {
        if (!edtAnswer.getText().toString().equals(mQuestions.get(questionIndex - 1).getAnswer())) {
            heartBar.setRating(--HEARTS);
            if (HEARTS <= 0) {
                layoutGame.setVisibility(View.GONE);
                layoutWin.setVisibility(View.GONE);
                layoutLoss.setVisibility(View.VISIBLE);
                return;
            }
            switch (HEARTS) {
                case 2:
                    showToastMessage(getString(R.string.msg_miss_game_1));
                    break;
                case 1:
                    showToastMessage(getString(R.string.msg_miss_game_2));
                    break;
            }
        }

        loadQuestion();
        edtAnswer.setText("");
    }

    @OnClick(R.id.btn_continue)
    public void onBtnContinueClicked() {
        showAlertDialog();
    }

    @OnClick(R.id.btn_finish)
    public void onBtnFinishClicked() {
        showAlertDialog();
    }
}
