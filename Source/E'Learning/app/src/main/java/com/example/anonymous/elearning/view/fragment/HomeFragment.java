package com.example.anonymous.elearning.view.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.anonymous.elearning.Constants;
import com.example.anonymous.elearning.R;
import com.example.anonymous.elearning.helper.GetDataFromFirebase;
import com.example.anonymous.elearning.view.activity.GameActivity;
import com.example.anonymous.elearning.view.activity.LearnActivity;
import com.example.anonymous.elearning.view.adapter.TopicAdapter;
import com.example.anonymous.elearning.view.model.TopicEntity;
import com.example.anonymous.elearning.view.model.WordEntity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Trieu on 09/11/2017.
 */

public class HomeFragment extends BaseFragment {

    @BindView(R.id.rv)
    RecyclerView rv;
    TopicAdapter adapter;
    GetDataFromFirebase data;
    FirebaseAuth mAuth;
    Fragment mFragment;
    int curNumberLevel=0,goal=1;
    ProgressBar progressBar;
    TextView txtPercent;
    private Context context;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TrangChuFragment.
     */
    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragment = this;
        mAuth = FirebaseAuth.getInstance();
        data = new GetDataFromFirebase(this);
    }

    @Override
    protected void onAttachToContext(Context context) {
        this.context = context;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }
    @Override
    protected void onMyCreateView() {
        prepareRecycleView();
        prepareAdapter();
        showBLoadingView("Loading");
        DatabaseReference authRef = FirebaseDatabase.getInstance()
                .getReference("users").child(mAuth.getCurrentUser().getUid()).child("process");

        authRef.addValueEventListener(new ValueEventListener() {
            @SuppressLint("ResourceAsColor")
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideBLoadingView();
                List<TopicEntity> list_topics = new ArrayList<>();
                curNumberLevel=0;
                for (DataSnapshot topic_entry : dataSnapshot.getChildren())
                {
                    TopicEntity topicEntity = new TopicEntity();
                    topicEntity.setKey(topic_entry.getKey());
                    topicEntity.setCurrent_level(Integer.parseInt(topic_entry.child("current_level").getValue().toString()));
                    topicEntity.setName(topic_entry.child("name").getValue().toString());
                    list_topics.add(topicEntity);

                    curNumberLevel=curNumberLevel+topicEntity.getCurrent_level();


                }

                adapter.setData(list_topics);

                Date currentTime = Calendar.getInstance().getTime(); //lay ngay hom nay
                int i=Calendar.getInstance().getTime().getDay();



                SharedPreferences myPref=getActivity().getSharedPreferences("pref", MODE_PRIVATE);
                SharedPreferences.Editor editor=myPref.edit();
                goal=myPref.getInt("goal",1);  // lay ngay cuoi luu trong app
                int day=myPref.getInt("day", 0);


                if(day!=i) // kiểm tra lại ngày mới
                {
                    // reset lại cái progress trong phần học của ông nhe?
                    curNumberLevel=0;
                    editor.putInt("day",i);
                    editor.commit();
                    day = i;

                }

                String percent=String.valueOf(curNumberLevel)+"/"+String.valueOf(goal);
                progressBar =(ProgressBar) myView.findViewById(R.id.progressBar);

                if(curNumberLevel<goal/3)
                {
                    progressBar.setProgressTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                }
                else
                {
                    progressBar.setProgressTintList(ColorStateList.valueOf(getResources().getColor(R.color.blueAmber)));
                }
                progressBar.setMax(goal);
                progressBar.setProgress(curNumberLevel);


                txtPercent=(TextView) myView.findViewById(R.id.txtPercentage);
                txtPercent.setText(percent);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideBLoadingView();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    private void prepareRecycleView(){
        LinearLayoutManager lm=new LinearLayoutManager(this.getActivity());
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(lm);
    }


    private void prepareAdapter() {
        if (adapter==null)
            adapter=new TopicAdapter(this);
        rv.setAdapter(adapter);

    }

    public void openTopic(final TopicEntity item,final int lv){
        String level=new String();
        level = "level"+lv;
        data.getDataLevel(item.getKey(), level, new GetDataFromFirebase.Callback() {
            @Override
            public void act(List<WordEntity> models) {
                item.setLevel1(models);
                Intent intent=new Intent(context,LearnActivity.class);
                intent.putExtra("Topic",item);
                intent.putExtra("userInfo", mAuth.getCurrentUser().getUid());
                intent.putExtra("levelChosen",lv);
                startActivity(intent);
            }
        });




    }

    public void openTest(TopicEntity item){
        Intent gameIntent = new Intent(context, GameActivity.class);
        gameIntent.putExtra(Constants.EXTRA_TOPIC,item);
        startActivity(gameIntent);
    }


}
