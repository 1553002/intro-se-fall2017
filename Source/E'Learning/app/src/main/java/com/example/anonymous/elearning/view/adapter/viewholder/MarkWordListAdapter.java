package com.example.anonymous.elearning.view.adapter.viewholder;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.anonymous.elearning.R;
import com.example.anonymous.elearning.view.activity.HomeActivity;
import com.example.anonymous.elearning.view.fragment.MarkedWordFragment;
import com.example.anonymous.elearning.view.model.WordEntity;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by nhan on 12/19/2017.
 */

public class MarkWordListAdapter extends RecyclerView.Adapter<MarkWordListAdapter.RecyclerViewHolder> {

    private List<WordEntity> data = new ArrayList<>();
    private Context mContext;
    private MarkedWordFragment mFragment;
    public MarkWordListAdapter(List<WordEntity> data, MarkedWordFragment Fragment) {
        this.data = data;
        this.mFragment=Fragment;
    }
    private  int curposition=0;

    public MarkWordListAdapter(Context context, List<WordEntity> wordEntityList)
    {
        this.mContext = context;
        data = wordEntityList;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.view_marked_word, parent, false);


        return new RecyclerViewHolder(view);
    }

    private void showDetail(WordEntity item, Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomAlertDialog);
        LayoutInflater factory = LayoutInflater.from(context);
        final View view = factory.inflate(R.layout.view_word_card,null);
        TextView word = (TextView)view.findViewById(R.id.wordswitcher);
        TextView spelling = (TextView)view.findViewById(R.id.spellswitcher);
        TextView defining = (TextView)view.findViewById(R.id.defineswitcher);
        ImageView image = (ImageView)view.findViewById(R.id.imageSwitcher);

        builder.setView(view);

        builder
                .setCancelable(true);
        word.setText(item.getWord());
        spelling.setText(item.getSpelling());
        defining.setText(item.getDefining());
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference gsReference = storage.getReferenceFromUrl(item.getRef());
        Glide.with(mContext)
                .using(new FirebaseImageLoader())
                .load(gsReference)
                .into(image);

        //dialog box details
        AlertDialog alert = builder.create();
        //alert.setTitle("Enter Details");
        alert.show();

    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        final WordEntity item = data.get(position);
        holder.word.setText(item.getWord());
        holder.mean.setText(item.getMeaning());
        holder.spell.setText(item.getSpelling());

        curposition=position;
/*        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference gsReference = storage.getReferenceFromUrl(item.getRef());
        Glide.with(mContext)
                .using(new FirebaseImageLoader())
                .load(gsReference)
                .into(holder.thumbnail);*/

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showDetail(item, v.getContext());
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public void removeItem(int position) {
        data.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(WordEntity item, int position) {
        data.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView word, mean, spell;
        public ImageView thumbnail;
        public RelativeLayout viewBackground, viewForeground;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            word = (TextView) itemView.findViewById(R.id.word);
            mean = (TextView) itemView.findViewById(R.id.mean);
            spell = (TextView) itemView.findViewById(R.id.spell);

            //thumbnail = itemView.findViewById(R.id.thumbnail);
            viewBackground = itemView.findViewById(R.id.view_background);
            viewForeground = itemView.findViewById(R.id.view_foreground);
        }
    }


}