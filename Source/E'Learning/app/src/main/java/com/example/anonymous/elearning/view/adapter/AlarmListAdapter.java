package com.example.anonymous.elearning.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.anonymous.elearning.R;
import com.example.anonymous.elearning.view.fragment.SetTimeFragment;
import com.example.anonymous.elearning.view.model.AlarmModel;

import java.util.List;

/**
 * Created by nhan on 12/1/2017.
 */

public class AlarmListAdapter extends BaseAdapter {
    public static String TAG = AlarmListAdapter.class.getSimpleName();
    private Context mContext;
    private List<AlarmModel> mAlarms;
    private  SetTimeFragment mFragment;

    public AlarmListAdapter(Context context, List<AlarmModel> alarms, SetTimeFragment fragment) {
        mContext = context;
        mAlarms = alarms;
        mFragment=fragment;

    }

    public void setAlarms(List<AlarmModel> alarms) {
        mAlarms = alarms;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if (mAlarms != null){
            return mAlarms.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        if (mAlarms != null) {
            return mAlarms.get(position);
        }
        return null;
    }


    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        if (mAlarms != null) {
            return mAlarms.get(position).getId();
        }
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ViewHolder viewholder;
        if(view == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.alarm_list_item, null, false);

            viewholder = new ViewHolder();
            viewholder.txtTime = (TextView) view.findViewById(R.id.alarm_item_time);
            viewholder.txtName = (TextView) view.findViewById(R.id.alarm_item_name);
            viewholder.btnToggle = (ToggleButton) view.findViewById(R.id.alarm_item_toogle);

            view.setTag(viewholder);

        }else{
            viewholder = (ViewHolder) view.getTag();
        }

        final AlarmModel model = (AlarmModel) getItem(position);

        viewholder.txtTime.setText(String.format("%02d : %02d", model.getTimeHour(), model.getTimeMinute()));
        viewholder.txtName.setText(model.getName());

        viewholder.btnToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                mFragment.setAlarmEnabled(Long.valueOf(model.getId()), isChecked);
            }
        });
        viewholder.btnToggle.setChecked(model.isEnabled());

        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mFragment.startAlarmDetailsActivity(Long.valueOf(model.getId()));
            }
        });

        view.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                Log.i(null, "abc1223");
                // TODO Auto-generated method stub
                mFragment.deleteAlarm(model.getId());
                return true;
            }
        });

        return view;
    }

    private void updateTextColor(TextView view, boolean isOn){
        if (isOn) {
            view.setTextColor(Color.GREEN);
        } else {
            view.setTextColor(Color.BLACK);
        }
    }

    private class ViewHolder {
        TextView txtTime;
        TextView txtName;
        ToggleButton btnToggle;
    }

}
