package com.example.anonymous.elearning.view.adapter.viewholder;

/**
 * Created by tranquocthaitrieu on 10/29/17.
 */

public interface ItemClickListener {
    void OnClickItem(int position);
}
