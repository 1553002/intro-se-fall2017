package com.example.anonymous.elearning.view.model;

import java.io.Serializable;

/**
 * Created by tranquocthaitrieu on 12/15/17.
 */

public class QuestionEntity implements Serializable {
    String question;
    String request;
    String answer;

    public QuestionEntity(String question, String request, String answer) {
        this.question = question;
        this.request = request;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
