package com.example.anonymous.elearning.view.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.anonymous.elearning.R;

import com.example.anonymous.elearning.view.adapter.viewholder.MarkWordListAdapter;
import com.example.anonymous.elearning.view.model.WordEntity;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by nhan on 12/26/2017.
 */

public class SetGoalFragment extends android.app.Fragment {

    private MarkWordListAdapter mAdapter;
    private Context mContext;
    RecyclerView list;
    FloatingActionButton btn_add;
    Fragment mFragment;
    View mContentView=null;
    List<WordEntity> mark_word;
    RecyclerView mRecyclerView;
    MarkWordListAdapter mRcvAdapter;
    List<WordEntity> data;

    EditText txtGoal;
    TextView txtShowGoal;
    public static SetGoalFragment newInstance() {

        Bundle args = new Bundle();

        SetGoalFragment fragment = new SetGoalFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // dbHelper=new AlarmDBHelper(activity);
        mContext=activity;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.set_goal_frag, container, false);
        txtGoal=(EditText) view.findViewById(R.id.set_goal);
        txtShowGoal=(TextView) view.findViewById(R.id.show_goal);
        txtGoal.setText(txtShowGoal.getText());
        txtGoal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                String goal=s.toString();
                if(!goal.isEmpty())
                txtShowGoal.setText(goal);
                else {txtShowGoal.setText("0");}

            }
        });


        return view;
    }

    @Override
        public void onStop() {
            super.onStop();

        String total=txtShowGoal.getText().toString();
        int totalLevel=Integer.parseInt(total);
        SharedPreferences.Editor editor =getActivity().getSharedPreferences("pref",MODE_PRIVATE).edit();
        editor.putInt("goal",totalLevel);
        editor.commit();




    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences myPref=getActivity().getSharedPreferences("pref", MODE_PRIVATE);
        int goal=myPref.getInt("goal",1);
        txtShowGoal.setText(String.valueOf(goal));
        txtGoal.setText(txtShowGoal.getText());
    }
}
