package com.example.anonymous.elearning.view.adapter;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.anonymous.elearning.view.fragment.GameFragment;
import com.example.anonymous.elearning.view.model.QuestionEntity;

import java.util.List;

/**
 * Created by Trieu on 8/31/2017.
 */

public class GameFragmentAdapter extends FragmentStatePagerAdapter {

    Context context;
    FragmentManager fm;
    Fragment[] fragments;

    public GameFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    public GameFragmentAdapter(Context context, FragmentManager fm, List<QuestionEntity> data) {
        super(fm);
        this.fm=fm;
        this.context=context;
        fragments=new Fragment[data.size()];
        for (int i=0;i<data.size();i++){
            fragments[i]= GameFragment.newInstance(data.get(i));
        }
    }


    public void clearAll(){
        for (int i=0;i<fragments.length;i++)
            fm.beginTransaction().remove(fragments[i]).commit();
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }
}