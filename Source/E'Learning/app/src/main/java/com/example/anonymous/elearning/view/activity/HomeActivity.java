package com.example.anonymous.elearning.view.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.anonymous.elearning.R;
import com.example.anonymous.elearning.view.customview.CircleTransform;
import com.example.anonymous.elearning.view.fragment.HomeFragment;
import com.example.anonymous.elearning.view.fragment.MarkedWordFragment;
import com.example.anonymous.elearning.view.fragment.SetGoalFragment;
import com.example.anonymous.elearning.view.fragment.SetTimeFragment;
import com.example.anonymous.elearning.view.model.WordEntity;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;

public class HomeActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "HomeActivity";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.container)
    CoordinatorLayout container;

    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    List<WordEntity> check;

    FirebaseDatabase db;
    DatabaseReference users;
    FirebaseUser currentUser;
    private final int FRAGMENTS=4;
    private final int FRAGMENT_HOME=0;
    private final int FRAGMENT_SET_TIME=2;
    private final int FRAGMENT_MARKED=1;
    private final int FRAGMENT_SET_GOAL=3;


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_home;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        prepareDatabase();
        prepareDrawer();
        prepareFragment();
        prepareUI();
        setToolbarTitle(FRAGMENT_HOME);
        showFrag(fragments[FRAGMENT_HOME]);

    }

    private void prepareDatabase(){
        db = FirebaseDatabase.getInstance();
        users = db.getReference("users");
        currentUser = FirebaseAuth.getInstance().getCurrentUser();



    }
    void getshow()
    {

    };

    private void prepareUI(){
        View view = navView.getHeaderView(0);
        //Gan text email
        TextView textView = view.findViewById(R.id.user_name);
        textView.setText(currentUser.getEmail());
        //Thiet lap anh profile
        Uri url=currentUser.getPhotoUrl();
        if (url!=null) {
            AppCompatImageView imageView = view.findViewById(R.id.profile_image);
            Picasso.with(HomeActivity.this).load(url).placeholder(R.drawable.person_flat)
                    .transform(new CircleTransform()).into(imageView);
        }
    }

    private void prepareDrawer(){
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navView.setCheckedItem(R.id.nav_home);
        navView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void setToolbarTitle(int index){
        getSupportActionBar().setTitle(getString(titles[index]));
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.nav_home:
                setToolbarTitle(FRAGMENT_HOME);
                showFrag(fragments[FRAGMENT_HOME]);
                break;
            case R.id.nav_set_time:
            {
                setToolbarTitle(FRAGMENT_SET_TIME);
                showFrag(fragments[FRAGMENT_SET_TIME]);
                break;
            }
            case R.id.nav_marked:
            {
                setToolbarTitle(FRAGMENT_MARKED);
                showFrag(fragments[FRAGMENT_MARKED]);
                break;
            }
            case R.id.nav_set_goal:
            {
                setToolbarTitle(FRAGMENT_SET_GOAL);
                showFrag(fragments[FRAGMENT_SET_GOAL]);
                break;
            }
            case R.id.nav_logout:
            {
                AuthUI.getInstance()
                        .signOut(this)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            public void onComplete(@NonNull Task<Void> task) {
                                // ...
                            }
                        });
                startActivity(new Intent(this,SignInActivity.class));
                finish();
                break;
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    int[] titles={R.string.nav_home,R.string.nav_marked,R.string.nav_set_time,R.string.nav_set_goal};
    Fragment[] fragments;

    void prepareFragment(){
        fragments = new Fragment[FRAGMENTS];
        //todo frgament bao gia
        fragments[FRAGMENT_HOME]= HomeFragment.newInstance();
        //todo frgament set time
        fragments[FRAGMENT_SET_TIME]= SetTimeFragment.newInstance();
//        //todo frgament marked
        fragments[FRAGMENT_MARKED]= MarkedWordFragment.newInstance();
//        //todo frgament set goal
        fragments[FRAGMENT_SET_GOAL]= SetGoalFragment.newInstance();
    }

   void showFrag(Fragment fragment){
        FragmentManager fm = getFragmentManager();
        fm.beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }




}
