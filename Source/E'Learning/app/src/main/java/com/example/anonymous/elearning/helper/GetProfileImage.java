package com.example.anonymous.elearning.helper;

import android.app.Activity;
import android.os.AsyncTask;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by CONG_CANH on 12/5/2017.
 */

public class GetProfileImage extends AsyncTask<String, Void, Void> {
    Activity context;

    public GetProfileImage(Activity context) {
        this.context = context;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
    /*public static Bitmap getFacebookProfilePicture(String userID) throws IOException {
                URL imageURL = new URL("https://graph.facebook.com/" + userID + "/picture?type=large");
                Bitmap bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());

                return bitmap;
            }*/
    @Override
    protected Void doInBackground(String... userID) {
        try {
            URL imageURL = new URL("https://graph.facebook.com/" + userID + "/picture?type=large");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
